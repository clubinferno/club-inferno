from django.db import models

class Sponsor(models.Model):
    """Modello degli sponsor"""

    nome_sponsor = models.CharField(max_length=30)
    descrizione = models.TextField(blank=True, null=True)
    logo = models.ImageField(blank=True, null=True)
    sito_web = models.CharField(max_length=120, blank=True, null=True)


    def __str__(self):
        return self.nome_sponsor

    class Meta:
        verbose_name = "Sponsor"
        verbose_name_plural = "Sponsors"
        