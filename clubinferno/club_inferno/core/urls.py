from django.urls import path
from .views import Homeview, contatti, ListaSponsor
urlpatterns = [
    path('', Homeview.as_view(), name='homepage'),
    path('contatti/', contatti, name='contatti'),
    path('sponsor/', ListaSponsor.as_view(), name="lista_sponsor"),

]