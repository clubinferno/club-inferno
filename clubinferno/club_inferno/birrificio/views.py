from django.shortcuts import render, get_object_or_404
from django.views.generic.list import ListView
from .models import Birrificio, Birra

class BirrificioListView(ListView):
    queryset = Birrificio.objects.all()
    # model = Birrificio
    template_name = "birrificio/lista_birrifici.html"
    context_object_name = "lista_birrifici"

class ListaBirre(ListView):
    model = Birra
    queryset = Birra.objects.order_by('birrificio_produttore','-offerta', 'esaurita')
    template_name = 'birrificio/lista_birre.html'

class ListaBirreDisponibili(ListView):
    model = Birra
    queryset = Birra.objects.order_by('-offerta').filter(esaurita=False)
    template_name = 'birrificio/birre_offerta_dispo.html'

def visualizzaBirre(request, pk):
    birrificio = get_object_or_404(Birrificio, pk=pk)
    birra = Birra.objects.filter(birrificio_produttore=birrificio)
    context = {"birrificio":birrificio, "birra":birra}
    return render(request, "birrificio/lista_birre_associate.html", context)

