from django.db import models
from django.urls import reverse
#from django.core.validators import MinValueValidator, MaxValueValidator


class Birrificio(models.Model):
    """Modello della birreria"""

    nome_birrificio = models.CharField(max_length=20)
    descrizione = models.TextField(null=True)
    nazionalita = models.CharField(max_length=15)
    sito_web = models.CharField(max_length=30, null=True)
    logo = models.ImageField(blank=True, null=True)

    def __str__(self):
        return self.nome_birrificio
    
    def get_absolute_url(self):
        return reverse('visualizza_birre', kwargs={"pk":self.pk})



    class Meta:
        verbose_name = "Birrificio"
        verbose_name_plural = "Birrifici"

class Birra(models.Model):
    """Modello della birra"""

    nome_birra = models.CharField(max_length=20)
    qualita = models.CharField(max_length=20)
    recensione = models.TextField()
    bottiglia = models.ImageField(blank=True, null=True)
    # voto_recensore = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(10)])
    birrificio_produttore = models.ForeignKey(Birrificio, on_delete=models.CASCADE,
                                             related_name="birreria_produttrice")
    data_inserimento = models.DateTimeField(auto_now_add=True)
    offerta = models.BooleanField(default=False)
    esaurita = models.BooleanField(default=False)
    


    def __str__(self):
        return self.nome_birra

    class Meta:
        verbose_name = "Birra"
        verbose_name_plural = "Birre"
