#!/usr/bin/env sh

# If done file doesn't exist
[ ! -f "done" ] && {
    ./manage.py collectstatic --noinput
    ./manage.py migrate
    adminPassword=$(cat /run/secrets/django-admin-password)
    echo "from django.contrib.auth.models import User; User.objects.create_superuser('$ADMIN_NAME', '$ADMIN_EMAIL', '$adminPassword')" | ./manage.py shell
    ./manage.py loaddata default_data/*
    rm -rf default_data
    touch "done"
}

uwsgi --socket uWSGIData/clubinferno.sock --module club_inferno.wsgi --chmod-socket=666