from django.shortcuts import render
from django.views.generic.list import ListView
from .models import Sponsor
from birrificio.models import Birra, Birrificio

# Create your views here.
# def homepage(request):
#     return render(request, 'core/homepage.html')

class Homeview(ListView):
    queryset = Birra.objects.order_by('birrificio_produttore')
    template_name = 'core/homepage.html'
    context_object_name = 'lista_birre_home'

def contatti(request):
    return render(request, 'core/contatti.html')

class ListaSponsor(ListView):
    model = Sponsor
    queryset = Sponsor.objects.all()
    template_name = 'core/sponsor.html'
