# Club Inferno

Club Inferno website build on Django, PostgreSQL, uWSGI and NGINX, shipped with Docker

## Configuration

Create the following files with the specified content:

* `secrets/adminPassword` : Password for the Django Administrator
* `secrets/dbpassword` : Password for the PostgreSQL Database
* `secrets/djangoSecretKey` : Your Django Secret Key

Other variables can be found on `.env` , among those:

* `DATABASE_NAME` : Name for the PostgreSQL Database
* `ADMIN_NAME` : Django Admin name
* `ADMIN_EMAIL` : Django Admin email

### SSL Certificate

A SSL cartificate it's needed in order to make the website work, place certificate and key in

* `nginx/ssl/clubinferno.crt`
* `nginx/ssl/clubinferno.key`

You can generate a self signed one with

``` sh
openssl req -nodes -x509 -newkey rsa:4096 -keyout clubinferno.key -out clubinferno.crt -days 365
```

## Start up the webapp

Just launch `docker-compose up -d` , default local page will be [127.0.0.1](http://127.0.0.1) on port 80/443
