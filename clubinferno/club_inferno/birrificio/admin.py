from django.contrib import admin
from .models import Birrificio, Birra

class BirrificioModelAdmin(admin.ModelAdmin):
    """Modello del birrificio per la pagina admin """
    model = Birrificio
    list_display = ["nome_birrificio", "logo", "nazionalita"]
    search_fields = ["nome_birrificio", "nazionalita"]

class BirraModelAdmin(admin.ModelAdmin):
    """Modello del birra per la pagina admin """
    model = Birra
    list_display = ["nome_birra", "qualita", "birrificio_produttore"]
    search_fields = ["nome_birra", "qualita", "birrificio_produttore"]

admin.site.register(Birrificio, BirrificioModelAdmin)
admin.site.register(Birra, BirraModelAdmin)
