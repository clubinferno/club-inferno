from django.contrib import admin
from .models import Sponsor

class SponsorModelAdmin(admin.ModelAdmin):
    """Modello admin per gli sponsor"""
    model = Sponsor
    list_display = ["nome_sponsor", "descrizione"]

admin.site.register(Sponsor, SponsorModelAdmin)
