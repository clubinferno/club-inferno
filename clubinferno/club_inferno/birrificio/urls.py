from django.urls import path
from .views import BirrificioListView, visualizzaBirre, ListaBirre, ListaBirreDisponibili
from django.conf.urls.static import static

urlpatterns = [
    path('lista_birrifici/', BirrificioListView.as_view(), name="lista_birrifici"),
    path('lista_birre_associate/<int:pk>/', visualizzaBirre, name="visualizza_birre"),
    path('birre/', ListaBirre.as_view(), name="lista_birre"),
    path('birre_dispo/', ListaBirreDisponibili.as_view(), name="lista_birre_dispo"),

]